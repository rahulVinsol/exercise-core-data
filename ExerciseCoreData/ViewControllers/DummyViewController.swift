//
//  DummyViewController.swift
//  ExerciseCoreData
//
//  Created by Rahul Rawat on 13/06/21.
//

import UIKit

class DummyViewController: UIViewController {
    private var person: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail" {
            let vc = segue.destination as! DetailViewController
            vc.person = person!
        }
    }
    
    func showDetail(ofPerson person: Person) {
        self.person = person
        performSegue(withIdentifier: "toDetail", sender: nil)
    }
}
