//
//  TableViewController.swift
//  ExerciseCoreData
//
//  Created by Rahul Rawat on 11/06/21.
//

import UIKit
import CoreData

protocol TableViewControllerDelegate {
    func showInsertVC()
    func rowTapped(person: Person)
}

class TableViewController: UITableViewController {
    
    var delegate: TableViewControllerDelegate? = nil
    
    private var deleteButton: UIBarButtonItem!
    private var addButtonItem: UIBarButtonItem!
    
    private lazy var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    private lazy var fetchRequestController: NSFetchedResultsController<Person> = {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Person.name), ascending: true)]
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        controller.delegate = self
        
        return controller
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        addButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        
        deleteButton = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(deleteTapped))
        deleteButton.tintColor = .red
        
        navigationItem.rightBarButtonItems = [editButtonItem, addButtonItem]
        
        do {
            try fetchRequestController.performFetch()
        } catch {
            
        }
    }
    
    @objc func addTapped(_ sender: UIButton) {
        delegate?.showInsertVC()
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        navigationItem.leftBarButtonItem = editing ? deleteButton : nil
        deleteButton.isEnabled = false
        
        addButtonItem.isEnabled = !editing
        
        super.setEditing(editing, animated: animated)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        fetchRequestController.sections?[0].numberOfObjects ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let person = fetchRequestController.object(at: indexPath)
        
        cell.textLabel?.text = person.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        .delete
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            delete(objectsAt: [indexPath])
        }
    }
    
    @objc func deleteTapped(_ sender: UIButton) {
        //to sort the rows in decreasing order so that the rows are deleted from bottom up to prevent inconsistent deletion
        let sortedIndexes = tableView.indexPathsForSelectedRows?.sorted { $0.row > $1.row }
        delete(objectsAt: sortedIndexes)
    }

    private func delete(objectsAt indexes: [IndexPath]?) {
        if let selectedRows = indexes {
            var index = 0
            
            var uuidsToDelete = [UUID]()
            
            while index < selectedRows.count {
                let indexPath = selectedRows[index]
                
                let person = fetchRequestController.object(at: indexPath)
                uuidsToDelete.append(person.imageName!)
                
                index += 1
            }
            
            if index > 0 {
                do {
                    let predicate = NSPredicate(format: "self.imageName IN %@", uuidsToDelete)
                    let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Person.fetchRequest()
                    fetchRequest.predicate = predicate
                    
                    let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                    batchDeleteRequest.resultType = .resultTypeObjectIDs
                    
                    let result = try context.execute(batchDeleteRequest) as! NSBatchDeleteResult
                    
                    let changes: [AnyHashable: Any] = [NSDeletedObjectsKey: result.result as! [NSManagedObjectID]]
                    NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [context])
                } catch {
                    
                }
            }
            changeDeleteButtonTitle()
        }

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isEditing {
            changeDeleteButtonTitle()
        } else {
            delegate?.rowTapped(person: fetchRequestController.object(at: indexPath))
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        changeDeleteButtonTitle()
    }
    
    private func changeDeleteButtonTitle() {
        let selectedRowsCount = tableView.indexPathsForSelectedRows?.count ?? 0
        
        deleteButton.title = "Delete" + (selectedRowsCount > 0 ? " (\(selectedRowsCount))" : "")
        
        deleteButton.isEnabled = selectedRowsCount > 0
    }
    
}

extension TableViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .none)
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        default:
            break
        }
    }
}
