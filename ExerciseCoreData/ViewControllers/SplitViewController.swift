//
//  SplitViewController.swift
//  ExerciseCoreData
//
//  Created by Rahul Rawat on 11/06/21.
//

import UIKit
import CoreData

class SplitViewController: UISplitViewController {
    
    private lazy var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private var lastPersonShown: Person? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ((viewControllers.first as? UINavigationController)?.viewControllers.first as? TableViewController)?.delegate = self        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(managedObjectContextObjectsDidChange), name:  Notification.Name.NSManagedObjectContextObjectsDidChange, object: context)
//        FileManager.default.checkImageDirectory()
    }
    
    @objc func managedObjectContextObjectsDidChange(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }

        if let deletes = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject>, deletes.count > 0 {
            let fileManager = FileManager.default
            
            for delete in deletes {
                if let person = (delete as? Person) {
                    fileManager.removeImage(withName: person.imageName!.uuidString)
                    
                    if person.imageName == lastPersonShown?.imageName {
                        popToRootDetailView(animated: true)
                    }
                }
            }
        }
    }

}

extension SplitViewController: TableViewControllerDelegate {
    func showInsertVC() {
        let createVC = AddUpdateViewController(isCreateMode: true, person: nil)
        createVC.delegate = self
        present(createVC, animated: true, completion: nil)
    }
    
    func rowTapped(person: Person) {
        lastPersonShown = person
        let splitDetailVC = viewControllers.last as? UINavigationController
        let viewControllers = splitDetailVC?.viewControllers
        
        let detailVC: DetailViewController?  = viewControllers?.first { vc in
            vc is DetailViewController
        } as? DetailViewController
        
        if let detailVC = detailVC {
            splitDetailVC?.popToViewController(detailVC, animated: true)
            detailVC.loadViewsAgain(withPerson: person)
        } else {
            splitDetailVC?.popToRootViewController(animated: false)
            (splitDetailVC?.viewControllers.first as! DummyViewController).showDetail(ofPerson: person)
        }
        
        let size = UIScreen.main.bounds.size
        if size.width < size.height {
            toggleMasterView()
        }
    }
}

extension SplitViewController: AddUpdateViewControllerDelegate {
    func personCreated(person: Person) {
        dismiss(animated: true, completion: nil)
    }
    
    func personUpdated(person: Person) {
        
    }
}

extension UISplitViewController {
    func toggleMasterView() {
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.preferredDisplayMode = .primaryHidden
        }
    }
    
    func popToRootDetailView(animated: Bool) {
        (viewControllers.last as? UINavigationController)?.popToRootViewController(animated: animated)
    }
}
