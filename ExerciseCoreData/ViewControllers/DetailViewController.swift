//
//  DetailViewController.swift
//  ExerciseCoreData
//
//  Created by Rahul Rawat on 11/06/21.
//

import UIKit
import MessageUI

class DetailViewController: UIViewController {
    
    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPosition: UILabel!
    @IBOutlet weak var lbWebsite: UILabel!
    @IBOutlet weak var lbShortDescription: UILabel!
    
    private lazy var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var person: Person!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let deleteButtonItem = UIBarButtonItem(image: UIImage(systemName: "trash"), style: .plain, target: self, action: #selector(deleteTapped))
        deleteButtonItem.tintColor = .red
        
        let updateButtonItem = UIBarButtonItem(image: UIImage(systemName: "pencil.circle"), style: .plain, target: self, action: #selector(updateTapped))
        
        let emailButtonItem = UIBarButtonItem(image: UIImage(systemName: "mail"), style: .done, target: self, action: #selector(mailTapped(_:)))
        
        let smsButtonItem = UIBarButtonItem(image: UIImage(systemName: "message"), style: .done, target: self, action: #selector(smsTapped(_:)))
        
        navigationItem.rightBarButtonItems = [updateButtonItem, deleteButtonItem, emailButtonItem, smsButtonItem]
        
        showDetails()
    }
    
    @objc func deleteTapped(_ sender: UIButton) {
        context.delete(person)
        
        do {
            try context.save()
        } catch {
            
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    @objc func updateTapped(_ sender: UIButton) {
        let updateVC = AddUpdateViewController(isCreateMode: false, person: person)
        updateVC.delegate = self
        navigationController?.pushViewController(updateVC, animated: true)
    }
    
    @objc func mailTapped(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            do {
                let mailVC = MFMailComposeViewController()
                mailVC.setSubject("Famous Person")
                mailVC.setMessageBody(person!.toString(), isHTML: false)
                
                let image = try UIImage.getImage(withFilename: person!.imageName!.uuidString)
                if let imageData = image.jpegData(compressionQuality: 1.0) {
                    mailVC.addAttachmentData(imageData, mimeType: "image/jpeg", fileName: "attachment.jpeg")
                }
                
                present(mailVC, animated: true)
            } catch {
                showErrorAlertVC(title: "Error", message: "Unable to send attachment", error: error)
            }
        } else {
            showErrorAlertVC(title: "Error", message: "Unable to send mail.", error: nil)
        }
    }
    
    @objc func smsTapped(_ sender: UIButton) {
        if MFMessageComposeViewController.canSendText() {
            let messageVC = MFMessageComposeViewController()
            messageVC.body = person!.toString()
            
            if MFMessageComposeViewController.canSendSubject() {
                messageVC.subject = "Famous Person"
            }
            
            if MFMessageComposeViewController.canSendAttachments() {
                do {
                    let image = try UIImage.getImage(withFilename: person!.imageName!.uuidString)
                    if let imageData = image.jpegData(compressionQuality: 1.0) {
                        messageVC.addAttachmentData(imageData, typeIdentifier: "image/jpeg", filename: "attachment.jpeg")
                    }
                } catch {
                    
                }
            }
            
            present(messageVC, animated: true)
        } else {
            showErrorAlertVC(title: "Error", message: "Unable to send SMS", error: nil)
        }
    }
    
    func loadViewsAgain(withPerson person: Person) {
        self.person = person
        showDetails()
    }
    
    private func showDetails() {
        do {
            ivImage.image = try UIImage.getImage(withFilename: person.imageName!.uuidString)
        } catch {
            
        }
        lbName.text = person.name
        lbPosition.text = person.position
        lbWebsite.text = person.website
        lbShortDescription.text = person.descriptionShort
    }
}

extension DetailViewController: AddUpdateViewControllerDelegate {
    func personCreated(person: Person) {
        
    }
    
    func personUpdated(person: Person) {
        navigationController?.popViewController(animated: true)
        loadViewsAgain(withPerson: person)
    }
}
