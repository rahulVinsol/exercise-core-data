//
//  AddUpdateViewController.swift
//  ExerciseCoreData
//
//  Created by Rahul Rawat on 11/06/21.
//

import UIKit
import PhotosUI
import MobileCoreServices

protocol AddUpdateViewControllerDelegate {
    func personCreated(person: Person)
    func personUpdated(person: Person)
}

class AddUpdateViewController: UIViewController {
    
    @IBOutlet weak var pickedImageView: UIImageView!
    @IBOutlet weak var pickImageButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var positionTextField: UITextField!
    @IBOutlet weak var websiteTextField: UITextField!
    @IBOutlet weak var shortDescriptionTextField: UITextField!
    @IBOutlet weak var createButton: UIButton!
    var scrollView: UIScrollView!
    var activeField: UITextField?
    
    private lazy var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    private var isCreateMode: Bool
    private var person: Person?
    
    private var wasNewImagePicked = false
    private var pickedImage: UIImage? = nil
    
    var delegate: AddUpdateViewControllerDelegate? = nil
    
    init(isCreateMode: Bool, person: Person?) {
        self.isCreateMode = isCreateMode
        self.person = person
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        isCreateMode = true
        self.person = nil
        super.init(coder: coder)
    }
    
    override func loadView() {
        super.loadView()
        if let nib = Bundle.main.loadNibNamed(String(describing: AddUpdateViewController.self), owner: self),
           let nibView = nib.first as? UIView {
            scrollView = UIScrollView()
            scrollView.frame = CGRect(x: CGFloat.zero, y: CGFloat.zero, width: view.frame.width, height: view.frame.height)
            
            view = scrollView
            
            scrollView.addSubview(nibView)
            scrollView.contentSize = CGSize(width: scrollView.frame.width, height: scrollView.frame.height)
            scrollView.layoutIfNeeded()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerForKeyboardNotifications()
        
        createButton.layer.cornerRadius = 10
        
        nameTextField.delegate = self
        positionTextField.delegate = self
        websiteTextField.delegate = self
        shortDescriptionTextField.delegate = self
        
        if !isCreateMode, let person = person {
            nameTextField.text = person.name
            positionTextField.text = person.position
            websiteTextField.text = person.website
            shortDescriptionTextField.text = person.descriptionShort
            
            createButton.setTitle("Update", for: .normal)
            
            do {
                pickedImage = try UIImage.getImage(withFilename: person.imageName!.uuidString)
                pickedImageView.image = pickedImage
            } catch {
                
            }
        }
    }
    
    @IBAction func pickImageTapped(_ sender: UIButton) {
        if #available(iOS 14, *) {
            var configuration = PHPickerConfiguration()
            configuration.filter = .images
            configuration.selectionLimit = 1
            
            let picker = PHPickerViewController(configuration: configuration)
            picker.delegate = self
            present(picker, animated: true, completion: nil)
        } else {
            showPicker()
        }
    }
    
    @IBAction func createTapped(_ sender: UIButton) {
        var errorOccured = false
        
        if nameTextField.text?.isEmpty != false {
            nameTextField.isError()
            errorOccured = true
        }
        
        if positionTextField.text?.isEmpty != false {
            positionTextField.isError()
            errorOccured = true
        }
        
        if websiteTextField.text?.isEmpty != false {
            websiteTextField.isError()
            errorOccured = true
        }
        
        if pickedImage == nil {
            pickImageButton.isError()
            pickedImageView.isError()
            errorOccured = true
        }
        
        if errorOccured {
            return
        }
        
        if isCreateMode {
            do {
                let uuid = UUID()
                
                try pickedImage!.save(withFilename: uuid.uuidString)
                
                let person = Person(context: context)
                person.name = nameTextField.text!
                person.position = positionTextField.text!
                person.website = websiteTextField.text!
                person.descriptionShort = shortDescriptionTextField.text
                person.imageName = uuid
                
                try context.save()
                
                delegate?.personCreated(person: person)
            } catch {
                
            }
        } else {
            do {
                if wasNewImagePicked {
                    FileManager.default.removeImage(withName: person!.imageName!.uuidString)
                    
                    try pickedImage!.save(withFilename: person!.imageName!.uuidString)
                }
                
                person!.name = nameTextField.text!
                person!.position = positionTextField.text!
                person!.website = websiteTextField.text!
                person!.descriptionShort = shortDescriptionTextField.text
                
                try context.save()
                
                delegate?.personUpdated(person: person!)
            } catch {
                
            }
        }
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWasShown(notification: NSNotification) {
        self.scrollView.isScrollEnabled = true
        let info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)

        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        let aRect : CGRect = CGRect(x: CGFloat.zero, y: CGFloat.zero, width: view.frame.width, height: view.frame.height - keyboardSize!.height)
        
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)) {
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }

    @objc func keyboardWillBeHidden(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
    }

    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }

    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
}

extension AddUpdateViewController: PHPickerViewControllerDelegate {
    
    @available(iOS 14, *)
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        dismiss(animated: true, completion: nil)
        
        if let itemProvider = results.first?.itemProvider, itemProvider.canLoadObject(ofClass: UIImage.self) {
            itemProvider.loadObject(ofClass: UIImage.self) { [weak self] image, error in
                let image = image as? UIImage
                DispatchQueue.main.async {
                    guard let self = self else { return }
                    if let image = image {
                        self.wasNewImagePicked = true
                        
                        self.pickedImage = image
                        self.pickedImageView.image = image
                    } else {
                        self.showErrorAlertVC(title: "Error", message: "Unable to pick image", error: error)
                    }
                }
            }
        } else {
            self.showErrorAlertVC(title: "Error", message: "Image cannot be loaded")
        }
    }
}

extension AddUpdateViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private func showPicker() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        imagePickerController.mediaTypes = [kUTTypeImage as String]
        imagePickerController.sourceType = .photoLibrary
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        
        switch info[UIImagePickerController.InfoKey.mediaType] as? String {
        
        case String(kUTTypeImage):
            let pickedImage = (info[UIImagePickerController.InfoKey.editedImage] ?? info[UIImagePickerController.InfoKey.originalImage]) as? UIImage
            if let image = pickedImage {
                self.wasNewImagePicked = true
                
                self.pickedImage = image
                self.pickedImageView.image = image
            }
        default:
            break
        }
    }
}


extension AddUpdateViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            positionTextField.becomeFirstResponder()
        case positionTextField:
            websiteTextField.becomeFirstResponder()
        case websiteTextField:
            shortDescriptionTextField.becomeFirstResponder()
        case shortDescriptionTextField:
            shortDescriptionTextField.resignFirstResponder()
            createButton.sendActions(for: .touchUpInside)
        default:
            break
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                           replacementString string: String) -> Bool {
        var maxLength = 0
        
        switch textField {
        case nameTextField:
            maxLength = 20
        case positionTextField:
            maxLength = 10
        case websiteTextField:
            maxLength = 50
        case shortDescriptionTextField:
            maxLength = 0
        default:
            break
        }
        
        if maxLength > 0 {
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
            return newString.count <= maxLength
        }
        
        return true
    }
}

extension UIView {
    func isError(baseColor: CGColor = UIColor.gray.cgColor, numberOfShakes shakes: Float = 4, revert: Bool = false) {
        let shake: CABasicAnimation = CABasicAnimation(keyPath: "position")
        shake.duration = 0.07
        shake.repeatCount = shakes
        shake.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        shake.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        self.layer.add(shake, forKey: "position")
    }
}
//UIEdgeInsets(top: 0.0, left: 0.0, bottom: 498.0, right: 0.0) (30.0, 548.0, 307.0, 34.0)
