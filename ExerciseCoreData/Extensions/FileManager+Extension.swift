//
//  FileManager.swift
//  ExerciseCoreData
//
//  Created by Rahul Rawat on 12/06/21.
//

import Foundation

extension FileManager {
    private var imageDirectoryName: String { "Images" }
    
    private func getFullFileName(with name: String) -> String {
        "\(name).jpg"
    }
    
    func getFullFilePath(with name: String) throws -> URL {
        try FileManager.default.getImageDirectory().appendingPathComponent(getFullFileName(with: name))
    }
    
    func getImageDirectory(createIfNecessary shouldCreate: Bool = true) throws -> URL {
        let imageDirectoryPath = self.urls(for: .documentDirectory, in: .userDomainMask).first!
            .appendingPathComponent(imageDirectoryName, isDirectory: true)
        
        if shouldCreate && !self.fileExists(atPath: imageDirectoryPath.path) {
            try self.createDirectory(at: imageDirectoryPath, withIntermediateDirectories: true, attributes: nil)
        }
        
        return imageDirectoryPath
    }
    
    func removeImage(withName name: String) {
        do {
            let imagePath = try getImageDirectory(createIfNecessary: false).appendingPathComponent(getFullFileName(with: name))
            
            try self.removeItem(at: imagePath)
        } catch {
            
        }
    }
    
    func clearImageDirectory() {
        do {
            let imageDirectoryPath = try getImageDirectory(createIfNecessary: false)
            
            try self.removeItem(at: imageDirectoryPath)
        } catch {
            
        }
    }
    
    func checkImageDirectory() {
        do {
            let imageDirectoryPath = try getImageDirectory(createIfNecessary: false)
            
            try self.contentsOfDirectory(atPath: imageDirectoryPath.path)
        } catch {
            
        }
    }
}
