//
//  UIViewController.swift
//  ExerciseCoreData
//
//  Created by Rahul Rawat on 14/06/21.
//

import UIKit

extension UIViewController {
    func showErrorAlertVC(title: String, message: String? = nil, error: Error? = nil) {
        let alert = UIAlertController(title: title, message: message ?? error?.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
