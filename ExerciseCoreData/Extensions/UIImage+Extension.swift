//
//  UIImage.swift
//  ExerciseCoreData
//
//  Created by Rahul Rawat on 12/06/21.
//

import UIKit

extension UIImage {
    static func getImage(withFilename filename: String) throws -> UIImage {
        let fileURL = try FileManager.default.getFullFilePath(with: filename)
        return UIImage(contentsOfFile: fileURL.path)!
    }
    
    func save(withFilename filename: String) throws {
        let fileURL = try FileManager.default.getFullFilePath(with: filename)
        if let data = self.jpegData(compressionQuality: 1.0) {
            try data.write(to: fileURL)
        }
    }
}
