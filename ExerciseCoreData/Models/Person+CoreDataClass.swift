//
//  Person+CoreDataClass.swift
//  ExerciseCoreData
//
//  Created by Rahul Rawat on 12/06/21.
//
//

import Foundation
import CoreData

@objc(Person)
public class Person: NSManagedObject {
    func toString() -> String {
        return "Name:- \(name ?? "")\n" +
        "Position:- \(position ?? "")\n" +
        "Short Description:- \(descriptionShort ?? "")\n" +
        "Website:- \(website ?? "")\n"
    }
}
